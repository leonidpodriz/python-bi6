number_1 = float(input("Введите первое число: "))
number_2 = float(input("Введите второе число: "))
operation = input('Введите операцию: ')

if operation == '+':
    print(number_1 + number_2)
elif operation == '-':
    print(number_1 - number_2)
elif operation == '/':
    print(number_1 / number_2)
elif operation == '*':
    print(number_1 * number_2)
else:
    print('Неизвестная операция!')
