# my_todo = list()
my_todo = [
    'Купить подарки на Новый год',
    'Сделать салат'
]
print(my_todo)

my_todo.append('Моя новая супер важная задача!')
my_todo.append('Ещё одна супер важная задача!')
print(my_todo)

print(my_todo[0])
print(my_todo[1])
print(my_todo[-1])

table = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
]

print(table[1][1])
