import random

print(random.random())  # [0, 1)
print(int(random.random() * 100))  # [0, 100)
print(random.randint(0, 100))  # [0, 100)
