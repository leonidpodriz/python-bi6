import random

temp = 20

while temp < 100:
    print('Температура воды сейчас:', temp)
    temp += random.randint(2, 7)

print("Финальная температура:", temp - temp % 100)
