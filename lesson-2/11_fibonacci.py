fibonacci_number = [0, 1]
numbers_count = 2

while numbers_count < 100:
    new_number = fibonacci_number[-1] + fibonacci_number[-2]
    fibonacci_number.append(new_number)
    numbers_count += 1

print(fibonacci_number)
