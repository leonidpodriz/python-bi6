index = 1
print('index =', index)

index *= 20
index /= 2
index += 0.5
print('index =', index)

x = 0
x += 10
x -= 1
x *= 3
x **= 2
x /= 9
print('x =', x)
