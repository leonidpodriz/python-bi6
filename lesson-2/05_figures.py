figure = input('Введите фигуру: ')
S = None

if figure == "прямоугольник":
    a = float(input('Введите первую сторону: '))
    b = float(input('Введите вторую сторону: '))
    S = a * b
elif figure == "квадрат":
    a = float(input('Введите сторону: '))
    S = a ** 2
elif figure == "круг":
    r = float(input('Введите радиус: '))
    S = 3.14 * r ** 2
elif figure == "треугольник":
    a = float(input('Введите сторону: '))
    h = float(input('Введите высоту: '))
    S = .5 * a * h

if S is not None:  # S != None
    if S > 100:
        print('Big', S)
    else:
        print('Small', S)
else:
    print("Не удалось посчитать площадь...")
