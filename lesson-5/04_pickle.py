import datetime
import pickle

data = {
    'name': 'Test#1',
    'birthday': datetime.datetime(1985, 10, 10, 13, 40, 50)
}
with open('data.pkl', 'wb') as file:  # wb - write binary
    pickle.dump(data, file)

# ...


with open('data.pkl', 'rb') as file:
    loaded_data = pickle.load(file)

print(loaded_data)
