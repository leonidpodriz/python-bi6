import json

data_to_json = {
    'key': 'value',
    'integer': 42,
    'float': 3.14,
    'none-type': None,
    'boolean': True,
    'inner-dict': {
        'list': [1, 2, 3, 4],
    },
}

with open('data.json', 'w') as file:
    json.dump(data_to_json, file, indent=2)

print(json.dumps(data_to_json, indent=2))
