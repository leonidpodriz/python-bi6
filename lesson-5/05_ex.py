import random
from pprint import pprint

EMPTY = '   '
STAR = ' * '
HEIGHT = 10
WIGHT = 10

grid = [
    [EMPTY for _ in range(WIGHT)]
    for _ in range(HEIGHT)
]

random_x_1 = random.randint(0, WIGHT - 1)
random_x_2 = random.randint(0, WIGHT - 1)
random_y_1 = random.randint(0, HEIGHT - 1)
random_y_2 = random.randint(0, HEIGHT - 1)

grid[random_y_1][random_x_1] = STAR
grid[random_y_2][random_x_2] = STAR

pprint(grid)
