from functools import reduce


def get_factorial_v2(number):
    if number == 1:
        return 1

    return get_factorial_v2(number - 1) * number


def get_factorial(number):
    return reduce(
        lambda x, y: x * y,
        range(2, number + 1)
    )


print(get_factorial(1000))
