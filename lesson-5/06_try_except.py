while True:
    number_one = None
    number_two = None
    result = None
    has_to_exit = False

    while number_one is None:
        try:
            number_one_value = input('Enter first number [or exit]: ')

            if number_one_value == 'exit':
                has_to_exit = True
                break

            number_one = float(number_one_value)
        except ValueError:
            print('Not valid number!')

    if has_to_exit is True:
        break

    while number_two is None:
        try:
            number_two = float(input('Enter second number: '))
        except ValueError:
            print('Not valid number!')

    operation = input('Enter an operation: ')

    if operation == '+':
        result = number_one + number_two
    elif operation == '-':
        result = number_one - number_two
    elif operation == '*':
        result = number_one * number_two
    elif operation == '/':
        try:
            result = number_one / number_two
        except ZeroDivisionError:
            print('Can not divide by zero!')
    else:
        print('Operation not found!')

    if result is not None:
        print('Result:', result)
    else:
        print('Result could not be calculated!')

# 4! = 2 * 3 * 4 = 24
