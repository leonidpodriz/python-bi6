# IO - input\output

flowers = []

with open('iris.csv') as file:
    file.readline()  # Чтобы пропустить первую строку

    for line in file:
        flower_data = line.strip('\n').split(',')
        flowers.append(
            {
                'sepal_length': float(flower_data[0]),
                'sepal_width': float(flower_data[1]),
                'petal_length': float(flower_data[2]),
                'petal_width': float(flower_data[3]),
                'species': flower_data[4]
            }
        )

sum_of_sepal_length = 0
count_of_flowers = 0
minimal_value = None
maximum_value = None

for flower in flowers:
    sepal_length = flower['sepal_length']

    if flower['species'] == 'virginica':
        sum_of_sepal_length += sepal_length
        count_of_flowers += 1

        if maximum_value is None or maximum_value < sepal_length:
            maximum_value = sepal_length

        if minimal_value is None or minimal_value > sepal_length:
            minimal_value = sepal_length

print('Avg:', sum_of_sepal_length / count_of_flowers)
print('Min:', minimal_value)
print('Max:', maximum_value)
# Найдите минимальное и максимальное значение для вида virginica
