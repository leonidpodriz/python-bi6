from itertools import permutations, product

names = ['Den', 'Kate', 'Nika', 'Alex']
ages = [29, 30, 24, 21, 34, 40]

print(list(product(names, ages)))
print(len(list(permutations(names))))
