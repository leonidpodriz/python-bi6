from string import ascii_lowercase

SHIFT = 13


def get_encoded_char(source_char, shift):
    try:
        encoded_index = (ascii_lowercase.index(source_char) + shift) % 26
        return ascii_lowercase[encoded_index]
    except ValueError:
        return source_char


def get_decoded_char(encoded_char, shift):
    try:
        decoded_index = (ascii_lowercase.index(encoded_char) - shift + 26) % 26
        return ascii_lowercase[decoded_index]
    except ValueError:
        return encoded_char


def get_encoded_text(text, shift):
    return ''.join(
        map(
            lambda char: get_encoded_char(char, shift),
            text,
        )
    )


def get_decoded_text(text, shift):
    return ''.join(
        map(
            lambda char: get_decoded_char(char, shift),
            text,
        )
    )


source_text = 'hello, world!'
encoded_text = get_encoded_text(source_text, SHIFT)
decoded_text = get_decoded_text(encoded_text, SHIFT)
print(encoded_text)
print(decoded_text)
