import datetime
import random

numbers = [
    random.randint(1, 10000) for _ in range(10000)
]
number_for_sorted = list(numbers)

start_of_bubble = datetime.datetime.now()

while True:
    was_changed = False

    for index in range(len(numbers) - 1):
        first_number = numbers[index]
        second_number = numbers[index + 1]

        if first_number > second_number:
            # first_number, second_number = second_number, first_number
            numbers[index] = second_number
            numbers[index + 1] = first_number
            was_changed = True

    if was_changed is False:
        break

end_of_bubble = datetime.datetime.now()
print('Bubble spend:', end_of_bubble - start_of_bubble)

start_of_sorted = datetime.datetime.now()
list(sorted(number_for_sorted))
end_of_sorted = datetime.datetime.now()
print('Sorted spend:', end_of_sorted - start_of_sorted)

print((end_of_bubble - start_of_bubble) / (end_of_sorted - start_of_sorted))
