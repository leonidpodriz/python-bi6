class Pen:

    def __init__(self, value, color):
        self.value = value
        self.color = color

    def write(self, text):
        for char in text:
            if self.value <= 0:
                break

            print(char, end='')

            if char != ' ':
                self.value -= 1

        # print()


my_pen_1 = Pen(100, 'red')
my_pen_2 = Pen(50, 'red')

text = '''
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad expedita facilis ipsa nobis pariatur perspiciatis quos rem unde! Aliquid atque cumque earum impedit omnis pariatur perferendis quis recusandae sapiente tenetur.
'''

for char in text:
    if my_pen_1.value > 0:
        my_pen_1.write(char)
    elif my_pen_2.value > 0:
        my_pen_2.write(char)
    else:
        print(f'ERROR! Can not write {char}')
