from collections import defaultdict

people = defaultdict(lambda: [])

people["Alex"] = ['3809xxxxxxxx', '3809xxxxxxxx']
people["Joe"] = ['3809xxxxxxxx', '3809xxxxxxxx', '3809xxxxxxxx']

print(people["Joe"])
print(people["Alex"])
print(people["No name"])
