contacts = {
    'Leonid Podriz': '380991591318',
    'Alex Gorban': None,
}

print(contacts)

contacts['Alex Gorban'] = '380991591319'

print(contacts)

print('Number:', contacts['Leonid Podriz'])

print('Есть ли Alex Gorban в контактах:', 'Alex Gorban' in contacts)
print('Есть ли Alex Non-Gorban в контактах:', 'Alex Non-Gorban' in contacts)

print('Все имена в контактах:', list(contacts.keys()))
print('Все телефоны в контактах:', list(contacts.values()))

for number in contacts.values():
    if '19' in number:
        print(number)

print(contacts)
print(contacts.pop('Alex Gorban'))
print(contacts)
