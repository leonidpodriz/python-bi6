numbers = set()
user_number = int(input('Enter an integer number: '))

for number in range(2, user_number + 1):
    print(f'Мы получили число {number} на проверку... ')
    is_prime = True

    for inner_number in range(2, number):
        print(f'Проверяем делится ли {number} на {inner_number}')
        if number % inner_number == 0:
            print(f'{number} делится на {inner_number}!')
            is_prime = False

    if is_prime is True and user_number % number == 0:
        print(f'Число {number} прошло все проверки успешно!')
        numbers.add(number)

print(numbers)
