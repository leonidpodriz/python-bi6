documents = [
    f'Document#{number}'
    for number
    in range(1, 13, 2)
]

# for number in range(1, 13):
#     documents.append(f'Document#{number}')

print(documents)

print(
    [
        number ** 2
        for number
        in range(1, 101)
    ]
)

hellos = [
    f'Привет, {name}!'
    for name
    in ['Настя', "Олег", "Коля", "Вика"]
    if 'я' not in name
]
print(
    hellos[1]
)
