import random

numbers = [
    random.randint(0, 100)
    for _
    in range(100)
]
print(numbers)
print('Минимальное число:', min(numbers))
print('Максимальное число:', max(numbers))
print('Длина коллекции:', len(numbers))
print('Сумма чисел коллекции:', sum(numbers))
print('Отсортированная коллекция:', sorted(numbers))
print('Отсортированная и отражённая коллекция:', list(reversed(sorted(numbers))))
