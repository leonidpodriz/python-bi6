parking_cars = {
    'KA1256VN',
    'KA1266VN',
    'KA1287VN',
    'AA1294KA',
}
black_list_cars = {
    'KA1256VN',
    'KA1266VN',
    'KA6456VN',
    'KA6459VN',
    'KA6449VN',
}
super_black_list_cars = {
    'KA1256VN',
    'KA6456VN',
    'KA6459VN',
    'KA6449VN',
    'SUPPER77',
}

vip = {
    'SUPPER77',
}

print(
    'In parking and in black list:',
    parking_cars.intersection(black_list_cars).intersection(super_black_list_cars)
)

print(
    'Full back list:',
    black_list_cars.union(super_black_list_cars)
)

print(
    'Full back list without VIPs:',
    black_list_cars.union(super_black_list_cars) - vip
)

print('Before add:', parking_cars)
parking_cars.add('NEW NUMBER')
print('After add:', parking_cars)
print('Is NEW NUMBER in parking', 'NEW NUMBER' in parking_cars)
print('Is SOME OTHER NUMBER in parking', 'SOME OTHER NUMBER' in parking_cars)

