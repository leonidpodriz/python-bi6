print({1, 2, 3}.difference({3, 4, 5}))
print({1, 2, 3} - {3, 4, 5})

print({1, 2, 3}.union({3, 4, 5}))
print({1, 2, 3} | {3, 4, 5})

print({1, 2, 3}.intersection({3, 4, 5}))
print({1, 2, 3} & {3, 4, 5})

print({1, 2, 3}.symmetric_difference({3, 4, 5}))
print({1, 2, 3} ^ {3, 4, 5})
