grades = '10,4,12,3,6,8,5,6,10,4'
grades_list = grades.split(',')
print(grades_list)
print(type(grades_list))
print('#'.join(grades_list))

numbers = []

for grade in grades_list:
    numbers.append(int(grade))

print(numbers)
print(
    [
        int(number)
        for number
        in grades_list
    ]
)
print(list(map(lambda n: int(n), grades_list)))

print(
    'Hello world c заменой буквы о на 0:', 'Hello, World'.replace('o', '0').replace('l', '1')
)
print('Hello, World'.replace('World', 'Dan.IT'))

hello = 'Hello, World!'

print(int('!!!!!10050!!!!'.strip('!')))
print('        Hello, World!      '.strip())
print('!!!!!!!Hello, World!!!!'.lstrip('!'))
print('!!!!!!!Hello, World!!!!'.rstrip('!'))
print('Hello, World!'[7:-1])

certificate = '--- BEGIN CERTIFICATE ... END CERTIFICATE ---'
print(
    certificate.startswith('--- BEGIN CERTIFICATE'),
    certificate.endswith('END CERTIFICATE ---'),
)
print(
    certificate.startswith('--- BEGIN PRIVATE KEY'),
    certificate.endswith('END PRIVATE KEY ---'),
)

print(hello.upper())
print(hello.lower())
print(hello.title())
print(hello.capitalize())

print(hello.title() == hello)
