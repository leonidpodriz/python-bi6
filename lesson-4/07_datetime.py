from datetime import datetime

start = datetime.now()

fibb = [0, 1]

for _ in range(20000):
    new_fibb = fibb[-1] + fibb[-2]
    fibb.append(new_fibb)

# print(fibb)
end = datetime.now()
print(end - start)
