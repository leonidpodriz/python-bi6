import math

names = input('Enter student names list separated by comma: ').split(',')
grades = {}

for name in names:
    grades[name] = [
        int(grade)
        for grade
        in input(f'Enter grades for {name}: ').split(',')
    ]

print(
    '\t|\t'.join(
        [
            'Name',
            'Max',
            'Min',
            'Avg',
            'Median'
        ]
    )
)

for name in grades.keys():
    student_grades = grades[name]
    sorted_student_grades = sorted(student_grades)
    grades_count = len(student_grades)

    # first_median_index = math.ceil(grades_count / 2)  # 6 / 2 = 3 -> 3
    # second_median_index = math.floor((grades_count / 2) - .5)  # (5 / 2) + .5 = 3 -> 4
    # print(first_median_index)
    # print(second_median_index)

    minimum_grade = sorted_student_grades[0]
    maximum_grade = sorted_student_grades[-1]
    average_grade = sum(sorted_student_grades) / grades_count
    # median_grade = (
    #     sorted_student_grades[first_median_index] +
    #     sorted_student_grades[second_median_index]
    # ) / 2

    if grades_count % 2 == 0:
        median_index_1 = int(grades_count / 2)
        median_index_2 = median_index_1 - 1
        median_grade = (
                               sorted_student_grades[median_index_1] +
                               sorted_student_grades[median_index_2]
                       ) / 2
    else:
        median_index = math.floor(grades_count / 2)
        median_grade = sorted_student_grades[median_index]

    print(
        '\t|\t'.join(
            [
                name,
                str(minimum_grade),
                str(maximum_grade),
                str(average_grade),
                str(median_grade)
            ]
        )
    )
