name = 'John Doe'
age = 29

print('Hello, %s. You are %i years old.' % (name, age))
print(
    'Hello, %(name)s. You are %(age)i years old.' % {
        'name': name,
        'age': age,
    }
)
print(
    'Hello, {name}. You are {age} years old.'.format(
        name=name,
        age=age,
    )
)
print(f'Hello, {name}. You are {age} years old.')
