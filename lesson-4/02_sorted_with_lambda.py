numbers = [1, 7, 4, 3, 7, 8, 3, 1, 6, 4, 3, 10, 78]


def from_big_to_small(number):
    return -number


print('Unsorted numbers:', numbers)
print('Sorted numbers:', sorted(numbers))
print('Custom sorted numbers #1:', sorted(numbers, key=from_big_to_small))
print('Custom sorted numbers #2:', sorted(numbers, key=lambda number: -number))
print('Custom sorted numbers #3:', sorted(numbers)[::-1])
