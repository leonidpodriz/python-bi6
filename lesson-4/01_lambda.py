def help_with_homework(task_number):
    print(f'Ищу задание под номером {task_number}...')
    print(f'Эх, не нашёл. Лучше у ментора спросите. :)')


tn = 1.6
help_with_homework(tn)

whoami = lambda: print('I am lambda!')
whoami()
whoami()
whoami()

is_ends_for_zero = lambda number: number % 10 == 0

print(is_ends_for_zero(10))
print(is_ends_for_zero(110))
print(is_ends_for_zero(150))
print(is_ends_for_zero(151))


