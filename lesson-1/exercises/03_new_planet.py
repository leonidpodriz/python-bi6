planet_name = input('Enter a planet name: ')
days_in_year = int(input('How many days in year: '))
seconds_in_day = 24 * 60 * 60
seconds_in_planet_year = seconds_in_day * days_in_year
print(
    'There are',
    seconds_in_planet_year,
    'seconds in one',
    planet_name,
    'year'
)
