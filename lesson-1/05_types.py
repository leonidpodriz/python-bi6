string_variable = 'this is a string'
integer_variable = 42
float_variable = 42.0
boolean_variable = True
none_variable = None

print(string_variable, type(string_variable))
print(integer_variable, type(integer_variable))
print(float_variable, type(float_variable))
print(boolean_variable, type(boolean_variable))
print(none_variable, type(none_variable))
